#ifndef MANAGER_H
#define MANAGER_H

#include "Engine.h"
#include "Tank.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class Manager {
	private:
		Engine engine;
		std::vector<Tank> tanks;
		std::ofstream file;
	public:
		Manager();
		void set_file_path(std::string);

		void start_engine();
		void stop_engine();
		void absorb_fuel(std::string);
		void give_back_fuel(std::string);

		void print_fuel_tank_count();
		
		void add_fuel_tank(std::string);
		void remove_fuel_tank(std::string); 
		void list_fuel_tanks();

		void list_connected_tanks();
		void print_total_fuel_quantity();
		void print_total_consumed_fuel_quantity();
		void print_tank_info(std::string);
		void fill_tank(std::string, std::string);
		
		void connect_fuel_tank_to_engine(std::string); 
		void disconnect_fuel_tank_from_engine(std::string); 
		
		void open_valve(std::string); 
		void close_valve(std::string);

		void wait(std::string);
		void wait_after(std::string);
		
		void break_fuel_tank(std::string); 
		void repair_fuel_tank(std::string); 
		
		void stop_simulation();
};

#endif